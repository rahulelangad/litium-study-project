﻿using Litium.Foundation.Modules.CMS.Routing;
using litium_studio_test_app.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.WebSites;
using Litium.Studio.Builders;


namespace litium_studio_test_app.Core.Definitions
{

    public static class PageInstanceFactory<T> where T : PageTypeDefinitionBase
    {
        public static T GetInstance()
        {
            var routePath = RouteUtilities.Parse(HttpContext.Current);

            if (routePath == null)
            {
                throw new ArgumentException(nameof(routePath));
            }
            return GetInstance(routePath.WebSite);
        }

        public static T GetInstance(Guid websiteId)
        {
            return GetInstance(ModuleCMS.Instance.WebSites[websiteId]);
        }
        public static T GetInstance(WebSite website)
        {
            var page = PageTypeDefinitionBase.GetSingleInstancePage<T>(website, ModuleCMS.Instance.AdminToken);

            if (page == null)
            {
                //throw new DoesNotExistException($"The page does not exists on the current website. Please create an instance of the {typeof(WebSiteSettings)} page type.");
            }

            return page.As<T>();
        }

    }


}