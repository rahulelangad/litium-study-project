﻿using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Products;
using litium_studio_test_app.Core.Services.Contracts;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.Core.Services
{
    public class CartItemViewModelBuilder : ICartItemViewModelBuilder
    {
        private readonly VariantService _variantService;
        private readonly IProductViewModelBuilder _productViewModelBuilder;

        public CartItemViewModelBuilder(VariantService variantService, IProductViewModelBuilder productViewModelBuilder)
        {
            _productViewModelBuilder = productViewModelBuilder;
            _variantService = variantService;
        }

        public CartItemViewModel CreateCartItemViewModel(OrderRowCarrier orderRowCarrier)
        {
            var variant = _variantService.Get(orderRowCarrier.ArticleNumber);

            if (variant == null)
                return null;

            var baseProduct = variant.GetBaseProduct();

            if (baseProduct == null)
                return null;

            var productViewModel = _productViewModelBuilder.CreateProductViewModelFromVariant(baseProduct, variant, DateTime.Now, CurrentState.Current.WebSiteID);

            
            var cartItemViewModel = new CartItemViewModel
            {
                ProductViewModel = productViewModel,
                Quantity = Decimal.ToInt32(orderRowCarrier.Quantity)
            };

            return cartItemViewModel;
        }

        public VariantViewModel CreateVariantViewModel(OrderRowCarrier orderRowCarrier)
        {
            var variant = _variantService.Get(orderRowCarrier.ArticleNumber);
            var variantModel = _productViewModelBuilder.CreateVariantViewModel(variant, DateTime.Now);
            return variantModel;
        }
    }
}