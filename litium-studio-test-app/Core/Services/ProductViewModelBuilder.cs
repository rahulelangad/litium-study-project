﻿using Litium.Products;
using Litium.Web.Models.Products;
using litium_studio_test_app.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web;
using litium_studio_test_app.Extensions;
using litium_studio_test_app.ViewModels;
using Litium.FieldFramework;
using Litium.Foundation.Extenssions;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Foundation.Security;
using litium_studio_test_app.Core.Helpers;

namespace litium_studio_test_app.Core.Services
{
    public class ProductViewModelBuilder : IProductViewModelBuilder
    {
        private readonly IPriceService _priceService;
        private readonly IStockService _stockService;
        private readonly ProductModelBuilder _productModelBuilder;
        private readonly IImageViewModelBuilder _imageViewModelBuilder;
        private readonly FieldDefinitionService _fieldDefinitionService;
        private readonly BaseProductService _baseProductService;
        private readonly VariantService _variantService;

        private const string _defaultVariantColorHex = "#ffffff";

        public ProductViewModelBuilder(IPriceService priceService, IStockService stockService, ProductModelBuilder productModelBuilder, IImageViewModelBuilder imageViewModelBuilder, FieldDefinitionService fieldDefinitionService, BaseProductService baseProductService, VariantService variantService)
        {
            _priceService = priceService;
            _stockService = stockService;
            _productModelBuilder = productModelBuilder;
            _imageViewModelBuilder = imageViewModelBuilder;
            _fieldDefinitionService = fieldDefinitionService;
            _baseProductService = baseProductService;
            _variantService = variantService;
        }

        public ProductViewModel CreateProductViewModelFromProductId(Guid productId, DateTime currentTime, Guid webSiteId)
        {
            var baseProduct = _baseProductService.Get(productId);
            return CreateProductViewModelFromBaseProduct(baseProduct, currentTime, webSiteId);
        }
        public ProductViewModel CreateProductViewModelFromVariantId(Guid variantId, DateTime currentTime, Guid webSiteId)
        {
            var variant = _variantService.Get(variantId);

            if (variant != null)
            {
                var baseProduct = _baseProductService.Get(variant.BaseProductSystemId);
                return CreateProductViewModelFromVariant(baseProduct, variant, currentTime, webSiteId);
            }
            return null;

        }
        public ProductViewModel CreateProductViewModelFromBaseProduct(BaseProduct baseProduct, DateTime currentTime, Guid webSiteId)
        {
            var productModel = _productModelBuilder.BuildFromBaseProduct(baseProduct, CurrentState.Current.WebSite);

            if (productModel == null)
            {
                return null;
            }

            var productViewModel = GetProductViewModel(productModel);

            var publishedVariants = baseProduct.GetPublishedVariants(webSiteId);
            productViewModel.Variants = publishedVariants.Select(x => CreateVariantViewModel(x, currentTime));
            //foreach (var currentVariant in publishedVariants.Select((value, i) => new { i, value }))
            //{
            //    if (currentVariant.value.GetUrl(CurrentState.Current.WebSiteID) == HttpContext.Current.Request.Url.AbsolutePath)
            //    {
            //        productViewModel.SelectedVariantIndex = currentVariant.i;
            //    }
            //}
            return productViewModel;
        }
        public ProductViewModel CreateProductViewModelFromVariant(BaseProduct baseProduct, Variant variant, DateTime currentTime, Guid webSiteId)
        {
            var productModel = _productModelBuilder.BuildFromBaseProduct(baseProduct, variant);

            if (productModel == null)
                return null;

            var productViewModel = GetProductViewModel(productModel);

            var publishedVariants = baseProduct.GetPublishedVariants(webSiteId);

            productViewModel.Variants = publishedVariants.Select(x => CreateVariantViewModel(x, currentTime));
            foreach (var currentVariant in publishedVariants.Select((value, i) => new { i, value }))
            {
                if (currentVariant.value.GetUrl(CurrentState.Current.WebSiteID) == variant.GetUrl(CurrentState.Current.WebSiteID))
                {
                    productViewModel.SelectedVariantIndex = currentVariant.i;
                }
            }

            return productViewModel;
        }

        public VariantViewModel CreateVariantViewModel(Variant variant, DateTime currentTime)
        {
            var productModel = _productModelBuilder.BuildFromVariant(variant);
            var variantViewModel = GetVariantViewModel(productModel, currentTime);
            return variantViewModel;
        }

        private ProductViewModel GetProductViewModel(ProductModel productModel)
        {
            if (productModel == null)
            {
                throw new Exception("productModel can not be null");
            }

            var imageIds = productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images);
            var images = new ReadOnlyCollection<ImageViewModel>(imageIds.Select(id => _imageViewModelBuilder.CreateImageViewModel(id)).ToList());

            var ecolabelId = productModel.GetValue<string>("ProductEcoLabel", CultureInfo.CurrentCulture);
            var ecolabelField = _fieldDefinitionService.Get("ProductEcoLabel");
            var ecolabelName = ecolabelField.GetTranslation(ecolabelId, CultureInfo.CurrentCulture);

            var productDate = productModel.GetValue<string>("HarmoneyReleaseDate", CurrentState.Current.Culture);

            var productViewModel = new ProductViewModel
            {
                Id = productModel.SelectedVariant.Id,
                //Currency = CurrentState.Current.ShoppingCart.Currency,
                UseVariantUrl = productModel.UseVariantUrl,
                Images = images,
                Description = productModel.GetValue<string>("_description", CurrentState.Current.Culture),
                //Name = productModel.GetValue<string>("ProductShortName", CurrentState.Current.Culture),
                Name = productModel.GetValue<string>("_name", CurrentState.Current.Culture),       
                ProductLongText = productModel.GetValue<string>("ProductLongText", CurrentState.Current.Culture),
                SeoDescription = productModel.GetValue<string>("_seoDescription", CurrentState.Current.Culture),
                SeoTitle = productModel.GetValue<string>("_seoTitle", CurrentState.Current.Culture),
                ProductWebPreambleTitle = productModel.GetValue<string>("ProductWebPreambleTitle", CurrentState.Current.Culture),
                ProductWebPreambleText = productModel.GetValue<string>("ProductWebPreambleText", CurrentState.Current.Culture),
                ProductQualityText = productModel.GetValue<string>("ProductQualityText", CurrentState.Current.Culture),
                ProductMeasureValue = productModel.GetValue<string>("ProductMeasureValue", CurrentState.Current.Culture),
                ProductMeasureUnit = productModel.GetValue<string>("ProductMeasureUnit", CurrentState.Current.Culture),
                Url = productModel.GetUrl(CurrentState.Current.WebSiteID),
                EcoLabel = ecolabelName,
                HarmoneyReleaseDate = productDate,
                ProductBaseColor = productModel.GetValue<string>("Colour", CurrentState.Current.Culture),
                InternalMemory = productModel.GetValue<string>("InternalMemory", CurrentState.Current.Culture),
                Specification = productModel.GetValue<string>("Specification", CurrentState.Current.Culture),
                

            };

            var culture = CurrentState.Current.Culture.GetRegionInfo();
            if (culture.Name == "US")
            {
                productViewModel.ProductMeasureValue = productModel.GetValue<string>("ProductMeasureValueUS", CurrentState.Current.Culture);
                productViewModel.ProductMeasureUnit = productModel.GetValue<string>("ProductMeasureUnitUS", CurrentState.Current.Culture);
            }

            return productViewModel;
        }

        private VariantViewModel GetVariantViewModel(ProductModel productModel, DateTime currentTime)
        {
            ReadOnlyCollection<ImageViewModel> images = null;
            if (productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images) != null)
            {
                var imageIds = productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images);
                images = new ReadOnlyCollection<ImageViewModel>(imageIds.Select(id => _imageViewModelBuilder.CreateImageViewModel(id)).ToList());
            }

            var variantViewModel = new VariantViewModel() 
            {
                Id = productModel.SelectedVariant.Id,
                //Name=productModel.GetValue<string>("_name",CurrentState.Current.Culture),
                Price = GetPriceViewModel(_priceService.GetPriceModels(productModel, currentTime)),
                CampaignPrice = _priceService.GetCampaignPrice(productModel, currentTime),
                StockStatusDescription = _stockService.GetStockStatusDescription(productModel.SelectedVariant),
                UseVariantUrl = productModel.UseVariantUrl,
                IsInStock = productModel.SelectedVariant.HasStocks(CurrentState.Current.WebSiteID, SecurityToken.CurrentSecurityToken.UserID),
                Images = images,
                Description = productModel.GetValue<string>("_description", CurrentState.Current.Culture),
                ProductLongText = productModel.GetValue<string>("ProductLongText", CurrentState.Current.Culture),
                SeoDescription = productModel.GetValue<string>("_seoDescription", CurrentState.Current.Culture),
                SeoTitle = productModel.GetValue<string>("_seoTitle", CurrentState.Current.Culture),
                ProductWebPreambleTitle = productModel.GetValue<string>("ProductWebPreambleTitle", CurrentState.Current.Culture),
                ProductWebPreambleText = productModel.GetValue<string>("ProductWebPreambleText", CurrentState.Current.Culture),
                Name = productModel.GetName(CultureInfo.CurrentCulture),
                Url = productModel.GetUrl(CurrentState.Current.WebSiteID),
                //BaseProduct = productModel.BaseProduct,
                Size = GetVariantSize(productModel),
                Color = GetVariantColor(productModel),
                VariantBaseColor = productModel.GetValue<string>("Colour", CurrentState.Current.Culture),
                InternalMemory = productModel.GetValue<string>("InternalMemory", CurrentState.Current.Culture),
                Specification = productModel.GetValue<string>("Specification", CurrentState.Current.Culture),
            };

            return variantViewModel;
        }

        private VariantColor GetVariantColor(ProductModel productModel)
        {
            var colorId = productModel.GetValue<string>("Color", CultureInfo.CurrentCulture);
            var colorText = productModel.GetValue<string>("ItemColorText", CultureInfo.CurrentCulture);
            var hex = ProductColorMap.GetColorCodeByName(string.Format("XX{0}", colorId)) ?? _defaultVariantColorHex;

            var variantColor = new VariantColor
            {
                Id = colorId,
                Name = colorText,
                Hex = hex
            };
            return variantColor;
        }

        private VariantSize GetVariantSize(ProductModel productModel)
        {
            var sizeField = _fieldDefinitionService.Get("Size");
            var sizeIds = productModel.GetValue<ReadOnlyCollection<string>>("Size", CultureInfo.CurrentCulture);
            var sizeId = sizeIds?.FirstOrDefault();
            var sizeName = sizeField.GetTranslation(sizeId, CultureInfo.CurrentCulture);

            var variantSize = new VariantSize
            {
                Id = sizeId,
                Name = sizeName
            };

            return variantSize;
        }

        private PriceViewModel GetPriceViewModel(IEnumerable<PriceModel> priceModels)
        {
            var result = new PriceViewModel();
            foreach (var priceModel in priceModels)
            {
                var priceItem = new PriceViewModel.PriceItem
                {
                    Price = priceModel.Price,
                    PriceInclVat = priceModel.Price * (1 + priceModel.VatPercentage),
                    VatPercentage = priceModel.VatPercentage,
                    MinimumQuantity = priceModel.MinimumQuantity,
                    PriceText = priceModel.PriceText
                };

                switch (priceModel.Type)
                {
                    case PriceType.Base:
                        if (result.BasePrice == null || result.BasePrice.Price > priceItem.Price)
                        {
                            result.BasePrice = priceItem;
                        }
                        break;
                    case PriceType.Discount:
                        if (result.DiscountPrice == null || result.DiscountPrice.Price > priceItem.Price)
                        {
                            result.DiscountPrice = priceItem;
                        }
                        break;
                    case PriceType.Club:
                        if (result.ClubPrice == null || result.ClubPrice.Price > priceItem.Price)
                        {
                            result.ClubPrice = priceItem;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return result;
        }
    }
}