﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Litium;
using Litium.Foundation.GUI;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.Relations;
using Litium.Foundation.Modules.Relations.Carriers;
using Litium.Foundation.Security;
using Litium.Foundation.Modules.Relations.Persons;
using Litium.Owin.Logging;

namespace litium_studio_test_app.Core.Services
{
    public class CustomerAccountLog
    {
        private static readonly ILog _logger = MethodBase.GetCurrentMethod().DeclaringType.Log();

        public static void RegisterCustomerAccountEvents(SecurityToken token)
        {
            //var eventManager = CurrentState.Current.Solution.EventManager;
            //eventManager.UserCustomPermissionCreated += EventCustomPermissionCreated;
            //eventManager.UserUpdated += EventCustomUpdated;
        }

        public static void AppendCurrentUserLog(string logText)
        {
            AppendUserLog(CurrentState.Current.User.ID, logText);
        }

        public static void AppendUserLog(Guid userId, string logText)
        {
            try
            {
                Person person = ModuleRelations.Instance.Persons.GetPerson(userId);
                if (person != null)
                {
                    PersonCarrier carrier = person.GetAsCarrier(false, false, false, false);
                    carrier.Comments = string.Format("{0}-{1}{2}{3}", DateTime.Now, logText, Environment.NewLine,
                        carrier.Comments);
                    person.UpdateFromCarrier(carrier, FoundationContext.Current.SystemToken);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Could not append to userlog, User ID: " + userId + " LogText: " + logText, ex);
            }
        }

        //private static void EventCustomPermissionCreated(Guid userid, Guid moduleid, int permissiontype, Guid key)
        //{
        //	User user = GetUser(userid);
        //	if (user != null)
        //	{
        //		_logger.Info(user.DisplayName + "Created");
        //	}
        //}

        //private static User GetUser(Guid userId)
        //{
        //	return Solution.Instance.Users.GetUser(userId);
        //}

        //private static void EventCustomUpdated(Guid userid)
        //{
        //	User user = GetUser(userid);
        //	if (user != null)
        //	{
        //		_logger.Info(user.DisplayName + "Updated");
        //	}
        //}
    }
}