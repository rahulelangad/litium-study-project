﻿using Litium.Foundation.Modules.Relations.Carriers;
using litium_studio_test_app.Core.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Foundation.GUI;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.Relations;
using Litium.Foundation.Security;
using Litium.Foundation.Modules.Relations.Persons;
using Litium.Foundation.Modules.Relations.FieldTemplates;

namespace litium_studio_test_app.Core.Services
{
    class CustomerService : ICustomerService
    {
        public void AddCustomer(PersonCarrier customerDetails)
        {
            //Generated password
            string generatedPassword = UserUtilities.GeneratePassword();

            var user = FoundationContext.Current.Solution.Users.GetUserFromLoginName(customerDetails.Email);
            if (user != null) return;
            var fieldTemplate = ModuleRelations.Instance.FieldTemplates.GetFieldTemplate("Customer");
            customerDetails.FieldTemplateID = fieldTemplate.ID;
            var person = ModuleRelations.Instance.Persons.CreatePerson(customerDetails, ModuleRelations.Instance.AdminToken);

            // Add person to customer group.
            //Group customerGroup = ModuleRelations.Instance.Groups.FirstOrDefault(g => g.FieldTemplateID == ModuleRelations.Instance.FieldTemplates.GetFieldTemplate("Customer").ID);
            //person.GroupMemberships.CreateMembership(customerGroup, ModuleRelations.Instance.AdminToken);

            // Send registration email to user

            EmailUtilities.NewUser(person.ID, customerDetails.Email, customerDetails.Login, generatedPassword);
        }

        public Litium.Foundation.Accounts.User LoginCustomer(string userName, string password)
        {
            Litium.Foundation.Accounts.User user = null;
            SecurityToken token;
            try
            {
                token = CurrentState.Current.Solution.LoginManager.Login(userName, password);
            }
            catch (ChangePasswordException ex)
            {
                return null;
            }
            if (token == null) return null;
            user = FoundationContext.Current.Solution.Users.GetUser(token.UserID);
            var accountLocked = false;
            if (user != null)
            {
                accountLocked = user.IsLockedOut;
            }
            if (accountLocked)
            {
                // Prevent locked customers to login
                CurrentState.Current.Solution.LoginManager.Logout(token);
            }
            return user;
        }
    }
}