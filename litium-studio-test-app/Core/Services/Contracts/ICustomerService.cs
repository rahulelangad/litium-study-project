﻿using Litium.Foundation.Modules.Relations.Carriers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace litium_studio_test_app.Core.Services.Contracts
{
  
        public interface ICustomerService
        {
            void AddCustomer(PersonCarrier customerDetails);
            Litium.Foundation.Accounts.User LoginCustomer(string userName, string password);
        }
  
}
