﻿using Litium.Foundation.Modules.ECommerce.Carriers;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace litium_studio_test_app.Core.Services.Contracts
{
    public interface ICartItemViewModelBuilder
    {
        CartItemViewModel CreateCartItemViewModel(OrderRowCarrier orderRowCarrier);
        VariantViewModel CreateVariantViewModel(OrderRowCarrier orderRowCarrier);
    }
}
