﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.Core.Services
{
    public class PriceModel
    {
        public decimal MinimumQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal VatPercentage { get; set; }
        public PriceType Type { get; set; }
        public string PriceText { get; set; }
    }
}