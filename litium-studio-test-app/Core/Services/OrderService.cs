﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using litium_studio_test_app.Core.Services.Contracts;
using litium_studio_test_app.ViewModels;
using Litium;
using Litium.Foundation.Modules;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ECommerce.Orders;
using Litium.Foundation.Modules.ECommerce.Payments;
using Litium.Foundation.Modules.ECommerce.Plugins.Payments;
using Litium.Foundation.Modules.ECommerce.ShoppingCarts;

namespace litium_studio_test_app.Core.Services
{
    class OrderService : IOrderService
    {


        public OrderService()
        {
        }

        public void PlaceOrder()
        {
            var orderCarrier = CurrentState.Current.ShoppingCart.OrderCarrier;
            if (orderCarrier != null && orderCarrier.PaymentInfo != null)
            {
                PaymentInfo[] paymentInfos = null;
                var order = CurrentState.Current.ShoppingCart.PlaceOrder(CurrentState.Current.Token, out paymentInfos);

                //Execute the order payment., the default implementation will have only one paymentInfo.
                //ExecutePayment();
            }
        }

      
        private void ExecutePayment()
        {

            //var addressCarrier=new AddressCarrier();
            // Create order
            PaymentInfo[] paymentInfos;
            Order order = CurrentState.Current.ShoppingCart.PlaceOrder(CurrentState.Current.Token, out paymentInfos);

            // NOTE: Call back url generated from shopping cart order carrier so we have to generate them after updating payment method.
            var checkoutFlowInfo = CurrentState.Current.ShoppingCart.CheckoutFlowInfo;
            if (checkoutFlowInfo != null)
            {
                //checkoutFlowInfo.ResponseUrl = GetCallbackUrl(true, order);
                //checkoutFlowInfo.CancelUrl = GetCallbackUrl(false, order);

                if (paymentInfos[0].PaymentProviderName == "PayPal")
                {
                    checkoutFlowInfo.ExecutePaymentMode = ExecutePaymentMode.Charge;
                }
            }

            // Execute payment.
            ExecutePayment(paymentInfos[0], order.ID);
        }

        private void ExecutePayment(PaymentInfo paymentInfo, Guid orderID)
        {
            try
            {
                ExecutePaymentResult result =
                    paymentInfo.ExecutePayment(CurrentState.Current.ShoppingCart.CheckoutFlowInfo, CurrentState.Current.Token);

                if (result.Success)
                {
                    //if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "PaymentForm"))
                    //{
                    if (!string.IsNullOrEmpty(CurrentState.Current.ShoppingCart.CheckoutFlowInfo.ResponseUrl))
                    {
                        // If payment successful user will be redirected to success url.
                        //Redirect(CurrentState.Current.ShoppingCart.CheckoutFlowInfo.ResponseUrl);
                    }
                    else
                    {
                        //Success URL wasn set for some reason. Try to set again.
                        //DebugUtilities.WriteDebugInfoToLog("ExecutePayment. Response URL not set", Page);
                        //Redirect(IsMobilePage ? WebSiteSettings.Instance.MobileCheckOutSuccessURLPage.GetUrlToPage(Guid.Empty, false) : WebSiteSettings.Instance.CheckOutSuccessURLPage.GetUrlToPage(Guid.Empty, false), true);
                    }
                    //}
                }
                else if (!string.IsNullOrEmpty(result.ErrorMessage))
                {
                    //DebugUtilities.WriteDebugInfoToLog(
                    //    string.Format("ExecutePayment Exception order: {0} result.ErrorMessage: {1}", paymentInfo.OrderID,
                    //                  result.ErrorMessage), Page);
                }
            }
            catch (ShoppingCartItemInvalidException cartInvalid)
            {
                //DebugUtilities.WriteDebugInfoToLog(
                //    string.Format("ExecutePayment ShoppingCartInvalidException order: {0} error: {1}", paymentInfo.OrderID,
                //                  cartInvalid.Message), Page);
            }
            catch (PaymentProviderException ex)
            {
                //DebugUtilities.WriteDebugInfoToLog(
                //    string.Format("ExecutePayment PaymentProviderException order:{0} error: {1}", paymentInfo.OrderID, ex.Message),
                //    Page);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                //DebugUtilities.WriteDebugInfoToLog(
                //    string.Format("ExecutePayment ArgumentOutOfRangeException order: {0} error: {1}", paymentInfo.OrderID, ex.Message),
                //    Page);
            }

            // Go to error page and display error. Error message can't be fetched from order payment info on error page.
            //string errorPageURL = (IsMobilePage ? WebSiteSettings.Instance.MobileCheckOutErrorPage.GetUrlToPage() : WebSiteSettings.Instance.CheckOutErrorPage.GetUrlToPage()) + "?" +
            //                      ParameterConstants.ECOM_ORDER_ID + "=" + orderID;
            //Redirect("/CheckoutError");
        }


    }
}