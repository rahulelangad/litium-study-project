﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using Litium.Foundation;
using Litium.Foundation.GUI;
using Litium.Foundation.Log;

namespace litium_studio_test_app.Core.Services
{
    public static class MailUtility
    {
        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="from">From address.</param>
        /// <param name="replyTo"> </param>
        /// <param name="to">To address</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="isBodyHtml">Specifies wether the mail contains HTML or not</param>
        /// <exception cref="SmtpException">Thrown when there is an SMTP error.</exception>
        public static void SendMail(string @from, string replyTo, string to, string subject, string body, bool isBodyHtml)
        {
            SendMail(@from, replyTo, to, string.Empty, string.Empty, subject, body, isBodyHtml, SmtpDeliveryMethod.PickupDirectoryFromIis);
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="from">From address.</param>
        /// <param name="replyTo"> </param>
        /// <param name="to">To address</param>
        /// <param name="cc">CC address</param>
        /// <param name="bcc">BCC address</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="isBodyHtml">Specifies wether the mail contains HTML or not</param>
        /// <param name="deliveryMethod">The delivery method.</param>
        /// <exception cref="SmtpException">Thrown when there is an SMTP error.</exception>
        public static void SendMail(string @from, string replyTo, string to, string cc, string bcc, string subject, string body, bool isBodyHtml, SmtpDeliveryMethod deliveryMethod)
        {
            try
            {
                SmtpClient mailClient = new SmtpClient();


                //Set mail client host to local host if empty
                if (!String.IsNullOrEmpty(FoundationContext.Current.SMTPServer))
                {
                    mailClient.Host = FoundationContext.Current.SMTPServer;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                else
                {
                    mailClient.Host = "127.0.0.1";
                    mailClient.DeliveryMethod = deliveryMethod;
                }

                if (String.IsNullOrEmpty(from))
                    throw new Exception("From address is required");

                #region GSD Artologik Changes
                // NOTE: Need to convert to iso-8859 because Artologik can't handle utf-8
                Encoding iso88591 = Encoding.GetEncoding("iso-8859-1");
                Encoding utf8 = Encoding.UTF8;
                body = iso88591.GetString(Encoding.Convert(utf8, iso88591, utf8.GetBytes(body)));
                subject = iso88591.GetString(Encoding.Convert(utf8, iso88591, utf8.GetBytes(subject)));
                #endregion GSD Artologik Changes

                using (MailMessage message = new MailMessage(from, to, subject, null))
                {
                    if (!String.IsNullOrEmpty(cc))
                    {
                        MailAddress ccAddress = new MailAddress(cc);
                        message.CC.Add(ccAddress);
                    }

                    if (!String.IsNullOrEmpty(replyTo))
                    {
                        MailAddress replyToAddress = new MailAddress(replyTo);
                        message.ReplyTo = replyToAddress;
                    }

                    if (!String.IsNullOrEmpty(bcc))
                    {
                        MailAddress bccAddress = new MailAddress(bcc);
                        message.Bcc.Add(bccAddress);
                    }

                    #region GSD Artologik Changes
                    message.Body = body;
                    message.SubjectEncoding = message.BodyEncoding = Encoding.GetEncoding("iso-8859-1");
                    message.IsBodyHtml = isBodyHtml;
                    mailClient.UseDefaultCredentials = true;
                    #endregion GSD Artologik Changes

                    mailClient.Send(message);
                }
            }
            catch (SmtpException e)
            {
                // Log to foundation log and then move on
                Solution.Instance.Log.CreateLogEntry("litium_studio_test_app.Core.Services.MailUtilities.Send", "Message: " + e.Message, LogLevels.FATAL);
                throw;
            }
            catch (Exception ex)
            {
                // Log to foundation log
                Solution.Instance.Log.CreateLogEntry("litium_studio_test_app.Core.Services.MailUtilities.Send", "Message: " + ex.Message, LogLevels.ERROR);
            }
        }
    }
}