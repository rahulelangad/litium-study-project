﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.Core.Services
{
    public enum PriceType
    {
        Base = 0,
        Discount = 1,
        Club = 2
    }
}