﻿using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Products;
using litium_studio_test_app.Core.Services.Contracts;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace litium_studio_test_app.Core.Services
{
    public class CartService : ICartService
    {
        private readonly ICartItemViewModelBuilder _cartItemViewModelBuilder;
        private readonly IProductViewModelBuilder _productViewModelBuilder;

        public CartService(ICartItemViewModelBuilder cartItemViewModelBuilder, IProductViewModelBuilder productViewModelBuilder)
        {
            _cartItemViewModelBuilder = cartItemViewModelBuilder;
            _productViewModelBuilder = productViewModelBuilder;
        }

        public CartViewModel GetCart()
        {
            var cart = new CartViewModel();
            var priceView = new List<PriceViewModel>();
            cart.Items = CurrentState.Current.ShoppingCart.OrderCarrier.OrderRows.Select(x => _cartItemViewModelBuilder.CreateCartItemViewModel(x)).ToList();
            #region Newely Added

            var variantService = IoC.Resolve<VariantService>();
            var total = cart.Items.Select(item => variantService.Get(item.ProductViewModel.Id)).Aggregate<Variant, decimal>(0, (current, selectedItem) => current + selectedItem.Prices.Select(x => x.Price).FirstOrDefault());
            cart.TotalProductPrice = total;

            #endregion

            return cart;
        }

        public void AddItem(string articleNumber, int quantity)
        {
            CurrentState.Current.ShoppingCart.Add(articleNumber, quantity, string.Empty, CurrentState.Current.WebSite.Language.ID);
        }

        public void RemoveItem(string articleNumber)
        {
            CurrentState.Current.ShoppingCart.RemoveProduct(articleNumber);
        }

        public void UpdateItem(string articleNumber, int newQuantity)
        {
            var cart = CurrentState.Current.ShoppingCart;
            var row = cart.OrderCarrier.OrderRows.Find(x => x.ArticleNumber.Equals(articleNumber) && !x.CarrierState.IsMarkedForDeleting);
            if (row == null) return;
            if (newQuantity == row.Quantity || newQuantity < 0) return;
            cart.UpdateRowQuantity(row.ID, newQuantity);
            cart.UpdateChangedRows();
        }
        private void AddDummyProducts()
        {
            if (CurrentState.Current.ShoppingCart.OrderCarrier != null &&
                CurrentState.Current.ShoppingCart.OrderCarrier.OrderRows.Count != 0) return;
            AddItem("74208_35_00000", 1);
            AddItem("74208_06_00000", 2);
        }


        private decimal GetTotalPriceInclVat(List<PriceViewModel> PriceView, List<CartItemViewModel> Items)
        {
            var total = 0.0M;
            var count = Items.Count();
            //for (int item = 0; item < Items.Count; item++) total = total + (PriceView[item].BasePrice.PriceInclVat * Items[item].Quantity);
            foreach (CartItemViewModel t in Items)
            {
                total = total + t.ProductViewModel.ProductPrice;
            }
            return Math.Round(total, 2);
        }


        private decimal GetTotalPrice(IReadOnlyList<PriceViewModel> price, IReadOnlyList<CartItemViewModel> Items)
        {
            var total = 0.0M;
            for (var item = 0; item < Items.Count; item++)
            {
                var min = price[item].BasePrice.PriceInclVat;
                if (price[item].DiscountPrice != null && min > price[item].DiscountPrice.PriceInclVat)
                    min = price[item].DiscountPrice.PriceInclVat;

                if (price[item].ClubPrice != null && min > price[item].ClubPrice.PriceInclVat)
                    min = price[item].ClubPrice.PriceInclVat;

                total += min * Items[item].Quantity;
            }
            return Math.Round(total, 2);
        }


        private decimal GetCommonVat(IReadOnlyList<PriceViewModel> price, IReadOnlyList<CartItemViewModel> items)
        {
            var total = 0.0M;
            for (var item = 0; item < items.Count; item++)
            {
                var combinedPrice = 0.0M;
                var priceInclVat = 0.0M;
                var priceExclVat = 0.0M;
                if (price[item].DiscountPrice != null)
                {
                    priceInclVat = price[item].DiscountPrice.PriceInclVat;
                    priceExclVat = price[item].DiscountPrice.Price;
                    combinedPrice = priceInclVat - priceExclVat;
                }
                else
                {
                    priceInclVat = price[item].BasePrice.PriceInclVat;
                    priceExclVat = price[item].BasePrice.Price;
                    combinedPrice = priceInclVat - priceExclVat;
                }

                total += items[item].Quantity * combinedPrice;
            }
            return Math.Round(total, 2);
        }
    }
}