﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using litium_studio_test_app.Interfaces;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Owin.InversionOfControl;
using Litium.Studio.Builders;
using litium_studio_test_app;
using litium_studio_test_app.Core.Definitions;
using litium_studio_test_app.Core.Services.Contracts;

namespace litium_studio_test_app.Core.Install
{
    public class ComponentInstaller : IComponentInstaller
    {
        public void Install(IIoCContainer container, Assembly[] assemblies)
        {
            //container.For<WebSiteStrings>()
            //    .UsingFactoryMethod(PageInstanceFactory<WebSiteSettings>.GetInstance)
            //    .RegisterAsScoped();

            container.For<WebSiteStrings>()
                .UsingFactoryMethod(() => CurrentState.Current.WebSite?.As<WebSiteStrings>())
                .RegisterAsScoped();

            // TODO: from accelerator, check if needed

            container.For<IStockService>().RegisterAsScoped();
            container.For<IPriceService>().RegisterAsScoped();
            container.For<IProductService>().RegisterAsScoped();
            container.For<IProductViewModelBuilder>().RegisterAsScoped();
            container.For<ICategoryPageViewModelBuilder>().RegisterAsScoped();
            container.For<IImageViewModelBuilder>().RegisterAsScoped();
            container.For<ICartService>().RegisterAsScoped();
            container.For<ICartItemViewModelBuilder>().RegisterAsScoped();
            container.For<ICategoryPageViewModelBuilder>().RegisterAsScoped();
            container.For<IAssortmentService>().RegisterAsScoped();


           
            container.For<IStockService>().RegisterAsScoped();
            container.For<IPriceService>().RegisterAsScoped();
            container.For<IProductService>().RegisterAsScoped();
            container.For<IAssortmentService>().RegisterAsScoped();
            container.For<IProductViewModelBuilder>().RegisterAsScoped();
            container.For<ICategoryPageViewModelBuilder>().RegisterAsScoped();
            container.For<ICartService>().RegisterAsScoped();
            container.For<ICartItemViewModelBuilder>().RegisterAsScoped();
            container.For<IImageViewModelBuilder>().RegisterAsScoped();
         
           

         

        }
    }
}