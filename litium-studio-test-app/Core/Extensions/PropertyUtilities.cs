﻿using Litium.Foundation.Modules;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Carriers;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.Core.Extensions
{
    public class PropertyUtilities
    {
        public const string MultiSelectDelimiter = ", ";

        /// <summary>
        ///     Gets the boolean property value of a boolean property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns> Property value or defaultValue if no property value exists.</returns>
        public static bool GetBooleanPropertyValue(PropertyCollection propertyCollection, string propertyName, bool defaultValue)
        {
            var property = propertyCollection.GetProperty(propertyName) as BooleanProperty;

            return (property != null && property.ValueCount != 0) && property.GetValue();
        }

        /// <summary>
        ///     Gets the boolean property value of a boolean property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>Property value or false if no property value exists.</returns>
        public static bool GetBooleanPropertyValue(PropertyCollection propertyCollection, string propertyName)
        {
            return GetBooleanPropertyValue(propertyCollection, propertyName, false);
        }

        /// <summary>
        ///     Gets the URL to the page represented by a image property.
        ///     Returns Url to the first image value (relative path including first "/"), example: "/BinaryLoader.axd?...".
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="maxHeight">maxHeight: Resize max height in pixels, -1 = don't resize (if maxWidth is also -1).</param>
        /// <param name="maxWidth">maxWidth: Resize max width in pixels, -1 = don't resize (if maxHeight is also -1).</param>
        /// <param name="minHeight">minHeight: Resize min height in pixels, -1 = don't resize (if minWidth is also -1).</param>
        /// <param name="minWidth">minWidth: Resize min width in pixels, -1 = don't resize (if minHeight is also -1)</param>
        /// <returns>
        ///     Url to the image, (relative path including first "/"), example: "/BinaryLoader.axd?...".
        /// </returns>
        public static string GetImagePropertyUrl(PropertyCollection propertyCollection, string propertyName, int maxHeight, int maxWidth, int minHeight, int minWidth)
        {
            var property = propertyCollection.GetProperty(propertyName) as ImageBaseProperty;

            // Return Guid.Empty if no property could be found, if it has no value or if the supplied user token is not allowed to read the target page.
            if (property == null || property.ValueCount == 0)
            {
                return string.Empty;
            }

            return property.GetUrlToImage(maxHeight, maxWidth, minHeight, minWidth);
        }

        /// <summary>
        ///     Gets the integer property value of an integer property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>Property value or the supplied default value if no property value exists.</returns>
        public static int GetIntegerPropertyValue(PropertyCollection propertyCollection, string propertyName, int defaultValue)
        {
            var property = propertyCollection.GetProperty(propertyName) as IntegerProperty;

            return (property == null || property.ValueCount == 0) ? defaultValue : property.GetValue();
        }

        /// <summary>
        ///     Gets the integer property value of an integer property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>Property value or Int32.MinValue if no property value exists.</returns>
        public static int GetIntegerPropertyValue(PropertyCollection propertyCollection, string propertyName)
        {
            return GetIntegerPropertyValue(propertyCollection, propertyName, Int32.MinValue);
        }

        /// <summary>
        ///     Gets the product group identifier property value.
        /// </summary>
        /// <param name="propertyCollection">The property collection.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>Guid.</returns>
        public static Guid GetCategoryIdPropertyValue(PropertyCollection propertyCollection, string propertyName)
        {
            var property = propertyCollection.GetProperty(propertyName) as ProductGroupIDProperty;

            return (property == null || property.ValueCount == 0) ? Guid.Empty : property.GetValue();
        }

        /// <summary>
        ///     Gets the page represented by a link internal property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="token">A SecurityToken.</param>
        /// <returns>A Page.</returns>
        public static Page GetLinkInternalPropertyPage(PropertyCollection propertyCollection, string propertyName, SecurityToken token)
        {
            return Page.GetFromID(Module<ModuleCMS>.Instance, GetLinkInternalPropertyValue(propertyCollection, propertyName, token), token);
        }

        /// <summary>
        ///     Gets the page info carrier represented by a link internal property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="token">A SecurityToken.</param>
        /// <returns>A PageInfoCarrier.</returns>
        public static PageInfoCarrier GetLinkInternalPropertyPageInfoCarrier(PropertyCollection propertyCollection, string propertyName, SecurityToken token)
        {
            return Module<ModuleCMS>.Instance.CacheManager.SiteMapCache.GetPage(GetLinkInternalPropertyValue(propertyCollection, propertyName, token));
        }

        /// <summary>
        ///     Gets the URL to the page represented by a link internal property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="token">A SecurityToken.</param>
        /// <returns>The URL to the page or string.Empty if no property value exists.</returns>
        public static string GetLinkInternalPropertyUrl(PropertyCollection propertyCollection, string propertyName, SecurityToken token)
        {
            PageInfoCarrier page = Module<ModuleCMS>.Instance.CacheManager.SiteMapCache.GetPage(GetLinkInternalPropertyValue(propertyCollection, propertyName, token));

            // Return the url of the page or string.Empty if no page was found.
            return page == null ? string.Empty : page.GetUrlToPage(CurrentState.Current.IsInAdministrationMode);
        }

        /// <summary>
        ///     Gets the value of a link internal property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="token">A SecurityToken.</param>
        /// <returns>Property value or Guid.Empty if no property value exists.</returns>
        public static Guid GetLinkInternalPropertyValue(PropertyCollection propertyCollection, string propertyName, SecurityToken token)
        {
            var property = propertyCollection.GetProperty(propertyName) as LinkInternalProperty;

            // Return Guid.Empty if no property could be found, if it has no value or if the supplied user token is not allowed to read the target page.
            if (property == null || property.ValueCount == 0 || !Module<ModuleCMS>.Instance.PermissionManager.UserHasPageReadPermission(token.UserID, property.GetValue(), true, true))
            {
                return Guid.Empty;
            }

            return property.GetValue();
        }

        /// <summary>
        ///     Gets the values of a link internal property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="token">A SecurityToken.</param>
        /// <returns>
        ///     A <see cref="List{Guid}" /> of the property values or an empty <see cref="List{Guid}" /> if no property value
        ///     exists.
        /// </returns>
        public static List<Guid> GetLinkInternalPropertyValues(PropertyCollection propertyCollection, string propertyName, SecurityToken token)
        {
            var pageIdList = new List<Guid>();
            var property = propertyCollection.GetProperty(propertyName) as LinkInternalProperty;

            if (property != null && property.ValueCount > 0)
            {
                for (int i = 0; i < property.ValueCount; i++)
                {
                    Guid pageId = property.GetValue(i);

                    if (Module<ModuleCMS>.Instance.PermissionManager.UserHasPageReadPermission(token.UserID, pageId, true, true))
                    {
                        pageIdList.Add(pageId);
                    }
                }
            }

            return pageIdList;
        }

        /// <summary>
        ///     Gets the string property value of a string property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>Property value or the supplied default value if no property value exists.</returns>
        public static string GetStringPropertyValue(PropertyCollection propertyCollection, string propertyName, string defaultValue)
        {
            var property = propertyCollection.GetProperty(propertyName) as StringProperty;

            return (property == null || property.ValueCount == 0) ? defaultValue : property.GetValue();
        }

        /// <summary>
        ///     Gets the string array property value of a string property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>Array property value</returns>
        public static List<string> GetStringArrayPropertyValue(
            PropertyCollection propertyCollection, string propertyName)
        {
            var property = propertyCollection.GetProperty(propertyName) as StringProperty;
            List<string> values = new List<string>();
            if ((property == null || property.ValueCount == 0))
            {
                return values;
            }
            for (int i = 0; i < property.ValueCount; i++)
            {
                var value = property.GetValue(i);
                if (!string.IsNullOrEmpty(value))
                {
                    values.Add(value);
                }
            }
            return values;
        }

        /// <summary>
        ///     Gets the string property value of a string property.
        /// </summary>
        /// <param name="propertyCollection">A property collection.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>Property value or string.Empty if no property value exists.</returns>
        public static string GetStringPropertyValue(PropertyCollection propertyCollection, string propertyName)
        {
            return GetStringPropertyValue(propertyCollection, propertyName, string.Empty);
        }
    }
}