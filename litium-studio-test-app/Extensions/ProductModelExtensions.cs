﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using JetBrains.Annotations;
using Litium.FieldFramework;
using Litium.Web.Models.Products;
using Litium.Products;
using Litium.Foundation.Modules.ExtensionMethods;

namespace litium_studio_test_app.Extensions
{
    public static class ProductModelExtensions
    {
        /// <summary>
        ///     Gets the products name.
        ///     If <see cref="ProductModel.UseVariantUrl" /> is true, priority is given to the field value from Variant.
        /// </summary>
        /// <param name="productModel">The product model.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns>System.String.</returns>
        public static string GetName([NotNull] this ProductModel productModel, CultureInfo cultureInfo)
        {
            return productModel.GetValue<string>(SystemFieldDefinitionConstants.Name, cultureInfo);
        }

        /// <summary>
        ///     Gets the URL.
        /// </summary>
        /// <param name="productModel">The product model.</param>
        /// <param name="webSiteSystemId">The web site system identifier.</param>
        /// <param name="currentCategory">The current category.</param>
        /// <returns>System.String.</returns>
        public static string GetUrl([NotNull] this ProductModel productModel, Guid webSiteSystemId, Category currentCategory = null)
        {
            return productModel.UseVariantUrl ? productModel.SelectedVariant.GetUrl(webSiteSystemId, currentCategory: currentCategory) : productModel.BaseProduct.GetUrl(webSiteSystemId, currentCategory: currentCategory);
        }

        /// <summary>
        ///     Determines whether this product is published.
        /// </summary>
        /// <param name="productModel">The product model.</param>
        /// <param name="webSiteSystemId">The web site system identifier.</param>
        /// <returns><c>true</c> if the specified web site system identifier is published; otherwise, <c>false</c>.</returns>
        public static bool IsPublished([NotNull] this ProductModel productModel, Guid webSiteSystemId)
        {
            return productModel.SelectedVariant.IsPublished(webSiteSystemId);
        }
    }
}