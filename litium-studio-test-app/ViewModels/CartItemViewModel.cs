﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class CartItemViewModel
    {
        public ProductViewModel ProductViewModel { get; set; }
        public int Quantity { get; set; }

        public   List<ProductViewModel> LstProductViewModels { get; set; }
           

        
    }
}