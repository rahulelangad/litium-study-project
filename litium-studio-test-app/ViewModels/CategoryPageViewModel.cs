﻿using litium_studio_test_app.ViewModels;
using System.Collections.Generic;
using litium_studio_test_app.Core.ViewModels;

namespace litium_studio_test_app.Interfaces
{
    public class CategoryPageViewModel
    {
        public CategoryViewModel CategoryViewModel { get; set; }
        public List<ProductViewModel> ProductList { get; set; }
        public List<MenuItemViewModel> MenuLinkList { get; set; }
    }
}