﻿using litium_studio_test_app.ViewModels;
using System.Collections.ObjectModel;

namespace litium_studio_test_app.Interfaces
{
    public class CategoryViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }  
        public ReadOnlyCollection<ImageViewModel> Images { get; set; }
        public string Url { get; set; }
    }
}