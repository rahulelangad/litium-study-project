﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class ImageViewModel
    {
        public string Url { get; set; }
        public string FileName { get; set; }
        public bool IsSelected { get; set; }
    }
}