﻿using System.Collections.Generic;

namespace litium_studio_test_app.ViewModels
{
    public class CustomerViewModel
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool RegisterUser { get; set; }
        public string CodeOfOrigin { get; set; }
        public bool IsCheckout { get; set; }
        public List<string> CodeOfOriginList { get; set; }
        public bool HasAlternateDeliveryAddress { get; set; }
        public string FirstNameAlternate { get; set; }
        public string SurNameAlternate { get; set; }
        public string Address1Alternate { get; set; }
        public string Address2Alternate { get; set; }
        public string ZipAlternate { get; set; }
        public string CityAlternate { get; set; }
        public string StateAlternate { get; set; }
        public bool IsClubMember { get; set; }
        public bool NeedNewsletter { get; set; }
    }
}