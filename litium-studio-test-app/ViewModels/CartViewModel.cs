﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class CartViewModel
    {
        public List<CartItemViewModel> Items { get; set; }

        public decimal TotalDeliveryCost { get; set; }

        public decimal TotalProductPrice { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal TotalSaved { get; set; }

        public decimal CommonVat { get; set; }
    }
}