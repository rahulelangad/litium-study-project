﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public ReadOnlyCollection<ImageViewModel> Images { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool UseVariantUrl { get; set; }
        public string ProductLongText { get; set; }
        public string SeoDescription { get; set; }
        public string SeoTitle { get; set; }
        public string ProductWebPreambleTitle { get; set; }
        public string ProductWebPreambleText { get; set; }
        public string ProductQualityText { get; set; }
        public string ProductMeasureValue { get; set; }
        public string ProductMeasureUnit { get; set; }
        public IEnumerable<VariantViewModel> Variants { get; set; }
        public int SelectedVariantIndex { get; set; }
        public string EcoLabel { get; set; }
        public string HarmoneyReleaseDate { get; set; }
        public bool IsInStock { get; set; }
        public string StockStatusDescription { get; set; }

        #region  Newly added fields

        public string ProductBaseColor { get; set; }
        public string InternalMemory { get; set; }
        public string Specification { get; set; }

        public decimal ProductPrice { get; set; }


        #endregion

        public ProductViewModel Copy()
        {
            return (ProductViewModel)this.MemberwiseClone();
        }
    }
}