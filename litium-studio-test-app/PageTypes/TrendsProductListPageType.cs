﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using litium_studio_test_app.Controllers;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.Pages;

namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.PRODUCT_CATALOG, nameof(TrendsProductListPageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = true,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = true,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true
    )]
    [Translation("Base Page", "sv-se")]
    [Translation("Base Page", "en-us")]
    [MvcTemplate("ProductPageType", "Images/Page.gif", typeof(TrendsProductListPageTypeController), "Index", Overwrite = true)]
    public class TrendsProductListPageType : PageTypeDefinitionBase
    {
        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page[] NewLinkInternal { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkInternal { get; set; }
        protected override List<string> RemovePropertyNames => new List<string>() { };
    }
}