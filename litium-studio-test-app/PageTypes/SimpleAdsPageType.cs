﻿using System.Collections.Generic;
using litium_studio_test_app.Controllers;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;


namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.REGULAR, nameof(SimpleAdsPageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = true,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = true,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true,
        PossibleParentPageTypes = "*")]
    [Translation("Ad Page", "sv-se")]
    [Translation("Ad Page", "en-us")]
    [MvcTemplate("AdPage1", "Images/Page.gif", typeof(SimpleAdsPageTypeController), "AdPage", Overwrite = true)]
    public class SimpleAdsPageType : PageTypeDefinitionBase
    {
        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page header", "en-us")]
        public virtual string PageHeader { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page sub header", "en-us")]
        public virtual string PageSubHeader { get; set; }

        [Property(PropertyCollectionTypes.SETTINGS, typeof(BooleanProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true)]
        [Translation("Sub header required", "en-us")]
        public virtual bool IsSubHeaderRequired { get; set; }

        protected override List<string> RemovePropertyNames
        {
            get { return new List<string>() {  }; }
        }
    }
}

