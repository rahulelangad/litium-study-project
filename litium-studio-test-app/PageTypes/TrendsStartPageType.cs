﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using litium_studio_test_app.Controllers;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;

namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.REGULAR, nameof(TrendsStartPageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = true,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = true,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true
    )]
    [Translation("Trends Home Page", "sv-se")]
    [Translation("Trends Home Page", "en-us")]

    [MvcTemplate("Trends Home Page", "Images/Page.gif", typeof(TrendsStartPageTypeController), "TrendsStartPage", Overwrite = true)]

    public class TrendsStartPageType : PageTypeDefinitionBase
    {
        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page header", "en-us")]
        public virtual string PageHeader { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page sub header", "en-us")]
        public virtual string PageSubHeader { get; set; }

        [Property(PropertyCollectionTypes.SETTINGS, typeof(BooleanProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true)]
        [Translation("Sub header required", "en-us")]
        public virtual bool IsSubHeaderRequired { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page[] LinkInteralPages { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToInteralPages { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToMainCategory1 { get; set; }
        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToMainCategory2 { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(ImageBaseProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToMainCategory1Image { get; set; }
        [Property(PropertyCollectionTypes.CONTENT, typeof(ImageBaseProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToMainCategory2Image { get; set; }

        protected override List<string> RemovePropertyNames => new List<string>() { };
    }
}