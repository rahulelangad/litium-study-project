﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.Relations.Carriers;
using litium_studio_test_app.Core.Services;
using litium_studio_test_app.Core.Services.Contracts;

namespace litium_studio_test_app.API
{
    public class CheckoutController : ApiController
    {
        private readonly ICustomerService _customerService;

        private readonly IOrderService _orderService;

        public CheckoutController(ICustomerService customerService, IOrderService orderService)
        {
            _customerService = customerService;
            _orderService = orderService;
        }

        [Route("ConfirmOrder")]
        [HttpPost]
        //public string ConfirmOrder(CartViewModel cartViewModel, CustomerViewModel customerViewModel)
        public string ConfirmOrder(CustomerViewModel customerViewModel)
        {

            PersonCarrier personData = ConvertModelToPersonObject(customerViewModel);
            if (customerViewModel.RegisterUser)
            {
                _customerService.AddCustomer(personData);
            }
            if (!CurrentState.Current.ShoppingCart.IsEmpty)
            {

                _orderService.PlaceOrder();

            }

            return "Success";
        }


        private PersonCarrier ConvertModelToPersonObject(CustomerViewModel customerViewModel)
        {
            var personCarrier = new PersonCarrier
            {
                Address = new Litium.Foundation.Modules.Relations.Carriers.AddressCarrier(),
                //Title = customerViewModel.Title.Trim(),
                FirstName = customerViewModel.FirstName.Trim(),
                LastName = customerViewModel.SurName.Trim(),
                Email = customerViewModel.Email.Trim().ToLower(),
                Login = customerViewModel.Email.Trim().ToLower()
            };

            // Person billing address
            var addressCarrier = personCarrier.Address;
            addressCarrier.Address1 = customerViewModel.Address1.Trim();
            //addressCarrier.Address2 = customerViewModel.Address2.Trim();
            addressCarrier.Zip = customerViewModel.Zip.Trim();
            addressCarrier.City = customerViewModel.City.Trim();
            addressCarrier.Country = customerViewModel.Country;
            addressCarrier.State = customerViewModel.State;
            addressCarrier.Email = customerViewModel.Email.Trim().ToLower();

            return personCarrier;
        }

        [Route("LoginUser")]
        [HttpPost]
        public bool LoginUser(string userName, string password)
        {
            Litium.Foundation.Accounts.User User = null;
            if (userName.Length > 0 && password.Length > 0) User = _customerService.LoginCustomer(userName, password);
            if (User == null) return false;
            else return true;
        }
    }
}
