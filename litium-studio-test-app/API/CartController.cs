﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS;

namespace litium_studio_test_app.API
{
    public class CartController : ApiController
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [Route("GetCart")]
        [HttpGet]
        public CartViewModel GetCart()
        {
            return _cartService.GetCart();
        }

        //[Route("AddItem/{articleNumber}/{quantity}")]
        [Route("AddItem")]
        [HttpGet]
        //public IHttpActionResult AddItem(string articleNumber = null, int quantity = 0)
        public IHttpActionResult AddItem(string articleNumber = null, int quantity = 0)
        {            
            _cartService.AddItem(articleNumber, quantity);
            return Ok(_cartService.GetCart());
        }

        [HttpGet]
        [Route("Clear")]
        public IHttpActionResult ClearShoppingCart()
        {
            CurrentState.Current.ShoppingCart.Clear();
            return Ok();
        }

        //[Route("RemoveItem/{articleNumber}")]
        [Route("RemoveItem")]
        [HttpGet]
        public CartViewModel RemoveItem(string articleNumber)
        {
            _cartService.RemoveItem(articleNumber);
            return _cartService.GetCart();
        }
    }
}
