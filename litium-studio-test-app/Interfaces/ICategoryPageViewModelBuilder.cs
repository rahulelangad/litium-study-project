﻿using Litium.Foundation.Modules.CMS.Pages;
using Litium.Products;
using System;

namespace litium_studio_test_app.Interfaces
{
    public interface ICategoryPageViewModelBuilder
    {
        CategoryPageViewModel CreateCategoryPageViewModel(Category category, Page page, Guid WebSiteId);
    }
}