﻿using Litium.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace litium_studio_test_app.Interfaces
{
    public interface IStockService
    {
        /// <summary>
        /// Gets the stock status description.
        /// </summary>
        /// <param name="inventorySystemId">The inventory system identifier.</param>
        /// <returns></returns>
        string GetStockStatusDescription(string sourceId = "");

        string GetStockStatusDescription(Variant variant, string sourceId = "");
    }
}
