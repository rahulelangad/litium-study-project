﻿using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace litium_studio_test_app.Interfaces
{
    public interface ICartService
    {
        CartViewModel GetCart();
        void AddItem(string articleNumber, int quantity);
        void RemoveItem(string articleNumber);
        void UpdateItem(string articleNumber, int newQuantity);
    }
}
