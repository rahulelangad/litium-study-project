﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.PageTypes;
using Litium.Foundation.Modules.ProductCatalog.Routing;
using Litium.Products;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Web.Products.Routing;
using Litium.Foundation.Modules.ExtensionMethods;
using System.Linq;
using System.Web.Services;
using litium_studio_test_app.ViewModels;
using Litium;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace litium_studio_test_app.Controllers
{
    public class TrendsCategoryPageController : LitiumController
    {
        private readonly ICategoryPageViewModelBuilder _categoryPageViewModelBuilder;
        private readonly WebSiteStrings _webSiteStrings;
        private readonly Lazy<Category> _category;

        public TrendsCategoryPageController(Litium.Web.Products.Routing.ProductPageData productPageData, CategoryService categoryService, ICategoryPageViewModelBuilder categoryPageViewModelBuilder, WebSiteStrings webSiteString)
        {
            _categoryPageViewModelBuilder = categoryPageViewModelBuilder;
            _webSiteStrings = webSiteString;
            _category = new Lazy<Category>(() => productPageData?.CategorySystemId == null ? null : categoryService.Get(productPageData.CategorySystemId.Value));
        }

        [HttpGet]
        public ActionResult Index(TrendsProductListPageType pageType, Page page)
        {
            var categoryPageModel = GetCategoryPageViewModel();

            var lstProductViewModels = new List<ProductViewModel>();
            var categoryPageViewModel = new CategoryPageViewModel();


            var varaintsListList = categoryPageModel.ProductList.Select(x => x.Variants).ToList();
            //var firstOrDefault = varaintsListList.FirstOrDefault();
            //if (firstOrDefault != null)
            //    lstProductViewModels.AddRange(from elemt in firstOrDefault
            //        let variantService = IoC.Resolve<VariantService>()
            //        let selectedVariant = variantService.Get(elemt.Id)
            //        select new ProductViewModel
            //        {
            //            Name = elemt.Name,
            //            Id = elemt.Id,
            //            Description = elemt.Description,
            //            ProductBaseColor = elemt.VariantBaseColor,
            //            Images = elemt.Images,
            //            ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault(),
            //            Url = elemt.Url,
            //            UseVariantUrl = elemt.UseVariantUrl,
            //            IsInStock =elemt.IsInStock,
            //            StockStatusDescription = elemt.StockStatusDescription,
            //            //Variants = elemt.StockStatusDescription
            //        });

           foreach (var item in varaintsListList)
                {
                    lstProductViewModels.AddRange(from elemt in item
                        let variantService = IoC.Resolve<VariantService>()
                        let selectedVariant = variantService.Get(elemt.Id)
                        select new ProductViewModel
                        {
                            Name = elemt.Name,
                            Id = elemt.Id,
                            Description = elemt.Description,
                            ProductBaseColor = elemt.VariantBaseColor,
                            Images = elemt.Images,
                            ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault(),
                            Url = elemt.Url,
                            UseVariantUrl = elemt.UseVariantUrl,
                            IsInStock = elemt.IsInStock,
                            StockStatusDescription = elemt.StockStatusDescription,
                            //Variants = elemt.StockStatusDescription
                        });
                } 
         




            categoryPageViewModel.ProductList = lstProductViewModels;
            return View(categoryPageViewModel);
        }
        private CategoryPageViewModel GetCategoryPageViewModel()
        {
            var categoryPageModel = _categoryPageViewModelBuilder
                .CreateCategoryPageViewModel(_category.Value, CurrentState.Current.Page, CurrentState.Current.WebSite.ID);

            return categoryPageModel;
        }
    }
}