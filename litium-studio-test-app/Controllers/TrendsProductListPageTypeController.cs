﻿using Litium.Foundation.Modules.CMS.Pages;
using Litium.Products;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using litium_studio_test_app.Core.Services;
using Litium.FieldFramework;
using litium_studio_test_app.ViewModels;
using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Web.Models.Products;
using System.Collections.ObjectModel;
using Litium.Web.Products.Routing;

namespace litium_studio_test_app.Controllers
{
    public class TrendsProductListPageTypeController : LitiumController
    {
        private readonly Guid _guid = Guid.NewGuid();
        private BaseProduct _baseProduct;
        private IEnumerable<Variant> _publishedVariants;
        private readonly IProductService _productService;
        private readonly IProductViewModelBuilder _productViewModelBuilder;
        private readonly Litium.Web.Products.Routing.ProductPageData _productPageData;
        private CategoryService _categoryService;
        private readonly BaseProductService _baseProductService;
        private readonly WebSiteStrings _webSiteStrings;
        private ProductPageViewModel _productPageViewModel;

        //public TrendsProductListPageTypeController(ProductPageViewModel productPageViewModel, WebSiteStrings webSiteStrings, BaseProductService baseProductService, CategoryService categoryService, IProductViewModelBuilder productViewModelBuilder, ProductPageData productPageData, IProductService productService, IEnumerable<Variant> publishedVariants, BaseProduct baseProduct)
        //{
        //    _productPageViewModel = productPageViewModel;
        //    _webSiteStrings = webSiteStrings;
        //    _baseProductService = baseProductService;
        //    _categoryService = categoryService;
        //    _productViewModelBuilder = productViewModelBuilder;
        //    _productPageData = productPageData;
        //    _productService = productService;
        //    _publishedVariants = publishedVariants;
        //    _baseProduct = baseProduct;
        //}

        public TrendsProductListPageTypeController(Litium.Web.Products.Routing.ProductPageData productPageData, CategoryService categoryService, BaseProductService baseProductService, IProductService productService, IProductViewModelBuilder productViewModelBuilder, WebSiteStrings webSiteStrings)
        {

            _productPageData = productPageData;
            _categoryService = categoryService;
            _baseProductService = baseProductService;
            _productViewModelBuilder = productViewModelBuilder;
            _productService = productService;
            _webSiteStrings = webSiteStrings;
        }

        // GET: ProductListPageType
        public ActionResult Index(TrendsProductListPageType productPagetype, Page currentPage)
        {
            _productPageViewModel = new ProductPageViewModel {ListVariantViewModel = new List<VariantViewModel>()};

            if (_productPageData.BaseProductSystemId == null) throw new Exception("invalid product");
            _baseProduct = _baseProductService.Get((Guid)_productPageData.BaseProductSystemId);


            //ViewBag.firstLink = productPagetype.LinkInternal?.GetUrlToPage();

            if (!string.IsNullOrEmpty(Request.QueryString["itemid"]))
            {
                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                var selectedVariant = _publishedVariants.FirstOrDefault(x => x.Id == Request.QueryString["itemid"]);

                var selectedVariantViewModel = GetVariantViewModel(selectedVariant);
                _productPageViewModel.ProductViewModel = new ProductViewModel
                {
                    Name = selectedVariantViewModel.Name,
                    Description = selectedVariantViewModel.Description,
                    Images = selectedVariantViewModel.Images,
                    ProductBaseColor = selectedVariantViewModel.VariantBaseColor,
                    Id = selectedVariantViewModel.Id,
                    InternalMemory = selectedVariantViewModel.InternalMemory,
                    Specification = selectedVariantViewModel.Specification
                };
                if (selectedVariant != null)
                    _productPageViewModel.ProductViewModel.ProductPrice =
                        selectedVariant.Prices.Select(x => x.Price).FirstOrDefault();


                return View("ProductDetails", _productPageViewModel);
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["method"]))
            {
                var cartService = IoC.Resolve<ICartService>();
                var cartItems = cartService.GetCart();

                var productModelBuilder = IoC.Resolve<ProductModelBuilder>();
                var cartItemViewModel = new CartItemViewModel();

                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                cartItemViewModel.LstProductViewModels = new List<ProductViewModel>();

                foreach (var item in cartItems.Items)
                {


                    var selectedVariant = _publishedVariants.FirstOrDefault(x => x.Id == item.ProductViewModel.Id);
                    var productModel = productModelBuilder.BuildFromVariant(selectedVariant);
                    var variantViewModel = GetVariantViewModel(productModel, DateTime.Now);

                    if (selectedVariant == null) continue;
                    {
                        var _productViewModel = new ProductViewModel
                        {
                            Id = variantViewModel.Id,
                            Name = variantViewModel.Name,
                            Images = variantViewModel.Images,
                            ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault(),
                            
                        };

                        cartItemViewModel.LstProductViewModels.Add(_productViewModel);
                    }
                }


                return View("Cart", cartItemViewModel);
            }

            else if (!string.IsNullOrEmpty(Request.QueryString["process"]))
            {
                var cartService = IoC.Resolve<ICartService>();
                var cartItems = cartService.GetCart();

                var productModelBuilder = IoC.Resolve<ProductModelBuilder>();
                var cartItemViewModel = new CartItemViewModel();

                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                cartItemViewModel.LstProductViewModels = new List<ProductViewModel>();

                foreach (var item in cartItems.Items)
                {


                    var selectedVariant = _publishedVariants.FirstOrDefault(x => x.Id == item.ProductViewModel.Id);
                    var productModel = productModelBuilder.BuildFromVariant(selectedVariant);
                    var variantViewModel = GetVariantViewModel(productModel, DateTime.Now);

                    if (selectedVariant == null) continue;
                    {
                        var _productViewModel = new ProductViewModel
                        {
                            Id = variantViewModel.Id,
                            Name = variantViewModel.Name,
                            Images = variantViewModel.Images,
                            ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault()
                        };

                        cartItemViewModel.LstProductViewModels.Add(_productViewModel);
                    }
                }


                return View("OrderSummary", cartItemViewModel);
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["checkout"]))
            {
                var cartService = IoC.Resolve<ICartService>();
                var cartItems = cartService.GetCart();

                var productModelBuilder = IoC.Resolve<ProductModelBuilder>();
                var cartItemViewModel = new CartItemViewModel();

                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                cartItemViewModel.LstProductViewModels = new List<ProductViewModel>();

                foreach (var item in cartItems.Items)
                {


                    var selectedVariant = _publishedVariants.FirstOrDefault(x => x.Id == item.ProductViewModel.Id);
                    var productModel = productModelBuilder.BuildFromVariant(selectedVariant);
                    var variantViewModel = GetVariantViewModel(productModel, DateTime.Now);

                    if (selectedVariant == null) continue;
                    {
                        var _productViewModel = new ProductViewModel
                        {
                            Id = variantViewModel.Id,
                            Name = variantViewModel.Name,
                            Images = variantViewModel.Images,
                            ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault(),

                        };

                        cartItemViewModel.LstProductViewModels.Add(_productViewModel);
                    }
                }


                return View("CheckoutCustomer", "");
            }
            else
            {

                var product = _productPageData.GetProductForWebSite(CurrentState.Current.WebSiteID);
                _productPageViewModel = GetPageViewModel();
                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                _productPageViewModel.CurrentWebsiteId = CurrentState.Current.WebSiteID.ToString();

                _productPageViewModel.ListVariantViewModel = new List<VariantViewModel>();

                foreach (var variantItem in _publishedVariants)
                {
                    var _variantViewModel = new VariantViewModel();
                    _variantViewModel = GetVariantViewModel(variantItem);
                    _productPageViewModel.ListVariantViewModel.Add(_variantViewModel);

                }

                return View("ProductDetails", _productPageViewModel);
            }

        }

        private NewVariantViewModel GetVariantViewModel(ProductModel productModel, DateTime currentTime)
        {
            ImageViewModelBuilder imageViewModelBuilder = new ImageViewModelBuilder();
            ReadOnlyCollection<ImageViewModel> images = null;
            if (productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images) != null)
            {
                var imageIds = productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images);
                images = new ReadOnlyCollection<ImageViewModel>(imageIds.Select(id => imageViewModelBuilder.CreateImageViewModel(id)).ToList());
            }

            var variantViewModel = new NewVariantViewModel()
            {
                Id = productModel.SelectedVariant.Id,
                Name = productModel.GetValue<string>("_name", CurrentState.Current.Culture),
                UseVariantUrl = productModel.UseVariantUrl,
                Images = images,
                Description = productModel.GetValue<string>("_description", CurrentState.Current.Culture),
                ProductLongText = productModel.GetValue<string>("ProductLongText", CurrentState.Current.Culture),
                SeoDescription = productModel.GetValue<string>("_seoDescription", CurrentState.Current.Culture),
                SeoTitle = productModel.GetValue<string>("_seoTitle", CurrentState.Current.Culture),
                ProductWebPreambleTitle = productModel.GetValue<string>("ProductWebPreambleTitle", CurrentState.Current.Culture),
                ProductWebPreambleText = productModel.GetValue<string>("ProductWebPreambleText", CurrentState.Current.Culture),
                VariantBaseColor = productModel.GetValue<string>("Colour", CurrentState.Current.Culture),
                InternalMemory = productModel.GetValue<string>("InternalMemory", CurrentState.Current.Culture),
                Specification = productModel.GetValue<string>("Specification", CurrentState.Current.Culture),

            };

            return variantViewModel;
        }



        //[HttpPost]
        public ActionResult ViewParticulrProduct()
        {
            return View("ProductDetails");
        }

        private ProductPageViewModel GetPageViewModel()
        {
            var productPageModel = new ProductPageViewModel
            {
                ProductViewModel = _productViewModelBuilder
                    .CreateProductViewModelFromBaseProduct(_baseProduct, ControllerContext.HttpContext.Timestamp,
                        CurrentState.Current.WebSite.ID)
            };


            return productPageModel;
        }

        private VariantViewModel GetVariantViewModel(Variant _variant)
        {
            var variantViewModel = _productViewModelBuilder.CreateVariantViewModel(_variant, DateTime.Now);
            return variantViewModel;
        }
    }
}