﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using litium_studio_test_app.PageTypes;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS.Pages;

namespace litium_studio_test_app.Controllers
{
    public class TrendsStartPageTypeController : Controller
    {
        private string _redirectInternalUrl;
        private string _firstredirectInternalUrl;
        private string _lLinkToMainCategory1;
        private string _lLinkToMainCategory2;
        private string _lLinkToMainCategory1Image;
        private string _lLinkToMainCategory2Image;
        // GET: StartPageType
        public ActionResult TrendsStartPage(TrendsStartPageType startPageType, Page currentPage)
        {
            _firstredirectInternalUrl = startPageType.LinkToInteralPages?.GetUrlToPage();
            _lLinkToMainCategory1 = startPageType.LinkToMainCategory1?.GetUrlToPage();
            _lLinkToMainCategory2 = startPageType.LinkToMainCategory2?.GetUrlToPage();

            var model = new AdFormModel()
            {
                RedirectInternalPageUrl = _firstredirectInternalUrl,
                LinkToMainCategory1 = _lLinkToMainCategory1,
                LinkToMainCategory2 = _lLinkToMainCategory2
            };


            Session["FirstName"] = "Rahul";

            return View(model);
        }
    }
}