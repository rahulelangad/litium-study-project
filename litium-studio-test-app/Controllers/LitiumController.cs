﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Security;
using Litium.Runtime.DependencyInjection;

namespace litium_studio_test_app.Controllers
{
    public class LitiumController : Controller
    {
        private readonly LazyService<ModuleCMS> _moduleCms = new LazyService<ModuleCMS>();
        protected readonly LazyService<ModuleECommerce> _moduleECommerce = new LazyService<ModuleECommerce>();
        private readonly LazyService<SecurityToken> _securityToken = new LazyService<SecurityToken>();
        private PropertyCollection _currentSettings;
        protected PropertyCollection CurrentSettings
        {
            get
            {
                if (_currentSettings == null)
                {
                    if (CurrentState.Current.IsWorkCopy)
                    {
                        _currentSettings = CurrentState.Current.WorkingCopy.Settings;
                    }
                    else if (CurrentState.Current.IsVersionCopy)
                    {
                        _currentSettings = CurrentState.Current.VersionCopy.Settings;
                    }
                    else if (CurrentState.Current.Page != null)
                    {
                        _currentSettings = CurrentState.Current.Page.Settings;
                    }
                }
                return _currentSettings;
            }
        }
        protected ModuleCMS ModuleCms => _moduleCms.Value;
        protected ModuleECommerce ModuleECommerce => _moduleECommerce.Value;
        protected SecurityToken SecurityToken => _securityToken.Value;


    }
}